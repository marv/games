# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

# NOTES: SPIDERMONKEY IS PATCHED

require gtk-icon-cache

SUMMARY="A free, open-source game of ancient warfare"
HOMEPAGE="https://play0ad.com"
DOWNLOADS="
    https://releases.wildfiregames.com/${PNV}-unix-build.tar.xz
    https://releases.wildfiregames.com/${PNV}-unix-data.tar.xz
"

LICENCES="
    Arev [[ note = [ Included DejaVu Fonts ] ]]
    bitstream-font [[ note = [ Included DejaVu Fonts ] ]]
    BSD-3 [[ note = [ build/premake/premake4 ] ]]
    CCPL-Attribution-ShareAlike-3.0 [[ note = [ binaries/data/mods/{art,audio} ] ]]
    GPL-2 [[ note = [
        source/tools/atlas
        Rest of binaries/data
        ] ]]
    GUST [[ note = [ Included TeXGyrePagella Fonts ] ]]
    ISC [[ note = [ source/lobby/pkcs5_pbkdf2.cpp ] ]]
    MIT [[ note = [
        build/premake/*.lua
        libraries/source/{fcollada,nvtt}
        source/lib
        source/third_party/mongoose
        ] ]]
    ZLIB [[ note = [ source/third_party/mikktspace ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    editor [[ description = [ Include Atlas scenario editor projects ] ]]
    lobby [[ description = [ Enable the multiplayer lobby ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        dev-lang/python:*[<3]
        virtual/pkg-config
    build+run:
        dev-libs/boost
        dev-libs/icu:=
        dev-libs/libsodium
        dev-libs/libxml2:2.0
        dev-libs/nspr[>=4.12]
        media-libs/SDL:2[X]
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/openal
        net-libs/enet
        net-libs/miniupnpc
        net-misc/curl
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcursor
        editor? ( x11-libs/wxGTK:3.0 )
        lobby? ( net-libs/gloox )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.0.23-alpha-flags.patch
    "${FILES}"/${PN}-missing-includes.patch
    "${FILES}"/${PN}-icu68.patch
)

src_unpack() {
    default

    ###### spidermonkey #####
    edo pushd "${WORK}"/libraries/source/spidermonkey
    edo tar xjf mozjs-38.2.1.rc0.tar.bz2
    edo mv mozjs-38.0.0 mozjs38
    edo popd
    #########################
}

src_prepare() {
    # build scripts don't work with Python 3, last checked: 0.0.23-alpha
    export PYTHON=/usr/$(exhost --build)/bin/python2

    default

    ###### fix premake #####
    edo pushd "${WORK}"/build/premake/premake4

    # The project uses an included version of premake (version 4.3.1 and 5.0.0-alpha12),
    # which doesn't seem available on premakes website.
    # 4.3 does not work, 4.4 (b5) should be tested. So here we build
    # premakes included version for now.
    emake -C build/gmake.unix

    # regenerate scripts.c so our patch applies
    edo ./bin/release/premake4 embed

    edo popd

    # rebuild premake again...
    emake -C "${WORK}"/build/premake/premake4/build/gmake.unix clean
    emake -C "${WORK}"/build/premake/premake4/build/gmake.unix
    ########################

    ###### spidermonkey #####
    edo pushd "${WORK}"/libraries/source/spidermonkey/mozjs38
    expatch \
        ../FixVersionDetection.diff \
        ../FixVersionDetectionConfigure.diff \
        ../FixZLibMozBuild.diff \
        ../FixTracelogger.diff
    edo cd js/src
    edo perl -i.bak -pe 's/(SHARED_LIBRARY_NAME\s+=).*/$1 '\''mozjs38-ps-release'\''/' \
        moz.build
    edo mkdir -p build-release
    edo popd
    #########################
}

src_configure() {
    local myconf=(
        --with-system-miniupnpc
        --minimal-flags
        --without-nvtt
        --without-pch
        --without-tests
        $(option editor "--atlas" "")
        $(option lobby "" "--without-lobby")
        --collada
        --bindir="/usr/$(exhost --target)/bin"
        --libdir="/usr/$(exhost --target)/lib/${PN}"
        --datadir="/usr/share/${PN}"
        )

    # run premake to create build scripts
    edo pushd "${WORK}"/build/premake
    edo ./premake4/bin/release/premake4 \
        --file="premake4.lua" \
        --outpath="../workspaces/gcc/" \
        --platform=$(option platform:amd64 "x64" "x32") \
        --os=linux \
        "${myconf[@]}" \
        gmake
    edo popd

    # fix tools invocation
    edo sed -i -e "s:pkg-config:${PKG_CONFIG}:g" \
        libraries/source/fcollada/src/Makefile \
        build/premake/pkgconfig/pkgconfig.lua \
        build/workspaces/gcc/* \
        libraries/source/spidermonkey/build.sh
    edo sed -i -e "s:@ar:@${AR}:" \
        -e "s:ranlib:${RANLIB}:" \
        libraries/source/fcollada/src/Makefile

    ###### spidermonkey #####
    # build bundled and patched spidermonkey-31
    #
    # spidermonkey build system requires that SHELL is always set.
    # It's missing sometimes in chroot environments, so force it here.
    export SHELL=/bin/sh
    edo pushd "${WORK}"/libraries/source/spidermonkey/mozjs38/js/src/build-release/
    ECONF_SOURCE="${WORK}/libraries/source/spidermonkey/mozjs38/js/src/" \
        econf \
            --hates=docdir \
            --hates=datarootdir \
            --enable-shared-js \
            --enable-gcgenerational \
            --disable-tests \
            --without-intl-api \
            --enable-optimize
    edo popd
    #########################
}

src_compile() {
    ###### spidermonkey #####
    edo pushd "${WORK}"/libraries/source/spidermonkey
    # TARGETS= from paludis environment breaks the build
    env -u TARGETS emake TOOLCHAIN_PREFIX="$(exhost --tool-prefix)" \
        -C mozjs38/js/src/build-release
    # copy headers for building
    edo mkdir -p include-unix-release
    edo cp -R -L mozjs38/js/src/build-release/dist/include/* \
        include-unix-release/
    edo mkdir -p lib/
    edo cp -L mozjs38/js/src/build-release/dist/lib/libmozjs38-ps-release.so \
        lib/libmozjs38-ps-release.so
    edo cp -L mozjs38/js/src/build-release/dist/lib/libmozjs38-ps-release.so \
        ../../../binaries/system/libmozjs38-ps-release.so
    edo popd
    #########################

    # build 3rd party fcollada
    emake -C libraries/source/fcollada/src

    # build 0ad
    emake -C build/workspaces/gcc verbose=1
}

src_install() {
    if optionq editor ; then
        newbin binaries/system/ActorEditor 0ad-ActorEditor
        exeinto /usr/$(exhost --target)/lib/${PN}
        doexe binaries/system/libAtlasUI.so
    fi

    # TODO: Use system fonts
    newbin binaries/system/pyrogenesis 0ad
    exeinto /usr/$(exhost --target)/lib/${PN}
    doexe binaries/system/libCollada.so
    doexe libraries/source/spidermonkey/mozjs38/js/src/build-release/dist/lib/*.so

    insinto /usr/share/${PN}
    doins -r binaries/data/*

    insinto /usr/share/icons/hicolor/128x128/apps/
    doins build/resources/${PN}.png
    insinto /usr/share/applications
    doins build/resources/0ad.desktop

    dodoc README.txt binaries/system/readme.txt
}

