# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_cmake_modules_REPOSITORY="https://github.com/asarium/cmake-modules.git"
SCM_librocket_REPOSITORY="https://github.com/asarium/libRocket.git"
SCM_SECONDARY_REPOSITORIES="
    cmake_modules
    librocket
"
SCM_EXTERNAL_REFS="
    cmake/external/rpavlik-cmake-modules:cmake_modules
    lib/libRocket:librocket
"

require github [ user=scp-fs2open project=fs2open.github.com tag=release_${PV//./_} force_git_clone=true ] \
    cmake \
    lua [ multibuild=false whitelist="5.1" ]

SUMMARY="FreeSpace2 Source Code Project"

LICENCES="fs2_open"
SLOT="0"
MYOPTIONS="
    vulkan
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        vulkan? ( sys-libs/shaderc )
    build+run:
        dev-libs/jansson[>=2.2]
        media/ffmpeg
        media-libs/SDL:2[X]
        media-libs/freetype:2
        media-libs/libpng:=
        media-libs/openal
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCOTIRE_DEBUG:BOOL=FALSE
    -DCOTIRE_VERBOSE:BOOL=FALSE
    -DENABLE_CLANG_TIDY:BOOL=FALSE
    -DFFMPEG_USE_PRECOMPILED:BOOL=FALSE
    -DFSO_BUILD_APPIMAGE:BOOL=FALSE
    -DFSO_BUILD_INCLUDED_LIBS:BOOL=FALSE
    -DFSO_BUILD_QTFRED:BOOL=FALSE
    -DFSO_BUILD_TESTS:BOOL=FALSE
    -DFSO_BUILD_TOOLS:BOOL=FALSE
    -DFSO_BUILD_WITH_FFMPEG:BOOL=TRUE
    -DFSO_BUILD_WITH_OPENGL:BOOL=TRUE
    -DFSO_FATAL_WARNINGS:BOOL=FALSE
    -DFSO_INSTALL_DEBUG_FILES:BOOL=FALSE
    -DFSO_RELEASE_LOGGING:BOOL=FALSE
    -DFSO_USE_LUAJIT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'vulkan FSO_BUILD_WITH_VULKAN'
)

src_configure() {
    # https://github.com/scp-fs2open/fs2open.github.com/issues/3182
    esandbox disable_net
    cmake_src_configure
    esandbox enable_net
}

src_install() {
    if ever is_scm ; then
        dobin bin/${PN}_21_5_0_x64
    else
        dobin bin/${PN}_21_4_0_x64
    fi
    dodoc "${CMAKE_SOURCE}"/Authors.md
}

pkg_postinst() {
    elog "${PN} requires a copy of the original FreeSpace2 resources. For more information see:"
    elog "https://wiki.hard-light.net/index.php/Manually_Installing_FreeSpace_2_Open"
}

