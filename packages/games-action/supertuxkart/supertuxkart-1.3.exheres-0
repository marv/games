# Copyright 2009-2015 Hong Hao <oahong@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="SuperTuxKart-${PV}-src"

require github [ project=stk-code release=${PV} suffix=tar.xz ] cmake
require gtk-icon-cache

SUMMARY="SuperTuxKart is a Free 3D kart racing game"
HOMEPAGE="https://supertuxkart.net/"

LICENCES="GPL-3 public-domain FIXME"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    sqlite [[ description = [ Manage server stats and ban lists ] ]]
    wiiremote [[ description = [ Support for wiimote input devices ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-cpp/mcpp
        dev-libs/angelscript[>=2.31]
        media-libs/freetype:2
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libsquish
        media-libs/libvorbis
        media-libs/openal
        media-libs/SDL:2
        net-libs/enet[>=1.3.4]
        net-misc/curl
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/harfbuzz
        sqlite? ( dev-db/sqlite:3 )
        wiiremote? ( app-emulation/wiiuse )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.1d] )
"
# bundled libraries:
#        tinygettext
#        bullet
#        dev-games/irrlicht[>=1.8]
#        SheemBidifor

UPSTREAM_DOCUMENTATION="${HOMEPAGE}FAQ"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_PREFIX:PATH=/usr
    -DSTK_INSTALL_BINARY_DIR:PAHT=/usr/$(exhost --target)/bin

    -DBUILD_RECORDER:BOOL=FALSE
    -DUSE_ASAN=OFF
    # Use libresolv.so instead of the bundled dns resolver dns.c
    -DUSE_DNS_C=OFF
    -DUSE_LIBBFD=OFF
    # Use OpenAL instead of the bundled MojoAL
    -DUSE_MOJOAL=OFF
    -DUSE_TSAN=OFF
    -DUSE_CRYPTO_OPENSSL=ON
    -DUSE_SYSTEM_ANGELSCRIPT=ON
    -DUSE_SYSTEM_ENET=ON
    -DUSE_SYSTEM_SQUISH=ON
    -DUSE_SYSTEM_WIIUSE=ON
)

CMAKE_SRC_CONFIGURE_OPTION_USES=(
    'sqlite SQLITE3'
    'wiiremote WIIUSE'
)

src_install() {
    cmake_src_install

    # cleanup
    edo find "${IMAGE}" -type f \( -name "*~" -or -name "*.sh" \) \
        -delete
    edo find "${IMAGE}"/usr/share/${PN}/data/po -type f \
        ! -name "*.po" -delete

    # Remove empty dir
    edo rmdir "${IMAGE}"/usr/share/supertuxkart/data/editor/maps
}

